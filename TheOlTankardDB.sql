CREATE DATABASE  IF NOT EXISTS `heroku_b09f050259d2ab8` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `heroku_b09f050259d2ab8`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: eu-cdbr-west-01.cleardb.com    Database: heroku_b09f050259d2ab8
-- ------------------------------------------------------
-- Server version	5.6.50-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alignment`
--

DROP TABLE IF EXISTS `alignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Tipo` (`Tipo`(191))
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alignment`
--

LOCK TABLES `alignment` WRITE;
/*!40000 ALTER TABLE `alignment` DISABLE KEYS */;
INSERT INTO `alignment` VALUES (1,'Lawful Good'),(2,'Neutral Good'),(3,'Chaotic Good'),(4,'Lawful Neutral'),(5,'Neutral'),(6,'Chaotic Neutral'),(7,'Lawful Evil'),(8,'Neutral Evil'),(9,'Chaotic Evil');
/*!40000 ALTER TABLE `alignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `armor`
--

DROP TABLE IF EXISTS `armor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `armor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `ac` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`(191))
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `armor`
--

LOCK TABLES `armor` WRITE;
/*!40000 ALTER TABLE `armor` DISABLE KEYS */;
INSERT INTO `armor` VALUES (1,'Padded',11,5),(2,'Leather',11,5),(3,'Studded leather',12,45),(4,'Hide',12,10),(5,'Chain shirt',13,50),(6,'Scale mail',14,50),(7,'Breastplate',14,400),(8,'Half plate',15,750),(9,'Ring mail',14,30),(10,'Chain mail',16,75),(11,'Splint',17,200),(12,'Plate',18,1500);
/*!40000 ALTER TABLE `armor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bag`
--

DROP TABLE IF EXISTS `bag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` int(11) DEFAULT NULL,
  `Armor` int(11) DEFAULT NULL,
  `Weapon` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `player` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Item_idx` (`Item`),
  KEY `Armor_idx` (`Armor`),
  KEY `Weapon_idx` (`Weapon`),
  KEY `player_idx` (`player`),
  CONSTRAINT `Armor` FOREIGN KEY (`Armor`) REFERENCES `armor` (`id`),
  CONSTRAINT `Item` FOREIGN KEY (`Item`) REFERENCES `items` (`id`),
  CONSTRAINT `Weapon` FOREIGN KEY (`Weapon`) REFERENCES `weapons` (`id`),
  CONSTRAINT `player` FOREIGN KEY (`player`) REFERENCES `characteroltankard` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bag`
--

LOCK TABLES `bag` WRITE;
/*!40000 ALTER TABLE `bag` DISABLE KEYS */;
/*!40000 ALTER TABLE `bag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cantrip`
--

DROP TABLE IF EXISTS `cantrip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cantrip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `dice` varchar(250) DEFAULT NULL,
  `Slot` varchar(250) DEFAULT NULL,
  `Description` mediumtext,
  `referencedClassxd` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cantrip`
--

LOCK TABLES `cantrip` WRITE;
/*!40000 ALTER TABLE `cantrip` DISABLE KEYS */;
INSERT INTO `cantrip` VALUES (1,'Acid splice','VS','1d6','Cantrip','You hurl a bubble of acid. Choose one creature within range, or choose two creatures within range that are within 5 feet of each other. A target must succeed on a Dexterity saving throw or take 1d6 acid damage.','Wizard'),(2,'Ray of frost','VS','1d8','Cantrip','A frigid beam of blue-white light streaks toward a creature within range. Make a ranged spell attack against the target. On a hit, it takes 1d8 cold damage, and its speed is reduced by 10 feet until the start of your next turn.','Wizard'),(3,'Light','VM','None','Cantrip','You touch one object that is no larger than 10 feet in any dimension. Until the spell ends, the object sheds bright light in a 20-foot radius and dim light for an additional 20 feet.','Cleric'),(4,'Sacred flame','VS','1d8','Cantrip','Flame-like radiance descends on a creature that you can see within range. The target must succeed on a Dexterity saving throw or take 1d8 radiant damage.','Cleric'),(5,'Fire bolt','VS','1d10','Cantrip','You hurl a mote of fire at a creature or object within range. Make a ranged spell attack against the target. On a hit, the target takes 1d10 fire damage.','Sorcerer'),(6,'Shocking grasp','VS','1d6','Cantrip','Lightning springs from your hand to deliver a shock to a creature you try to touch. Make a melee spell attack against the target. You have advantage on the attack roll if the target is wearing armor made of metal.','Sorcerer'),(7,'Eldritch Blast','VS','1d10','Cantrip','A beam of crackling energy streaks toward a creature within range. Make a ranged spell attack against the target. On a hit, the target takes 1d10 force damage. The spell creates more than one beam when you reach higher levels: two beams at 5th level','Warlock'),(8,'Poison Spray','VS','1d12','Cantrip','You extend your hand toward a creature you can see within range and project a puff of noxious gas from your palm. The creature must succeed on a Constitution saving throw or take 1d12 poison damage. This spell\'s damage increases by 1d12 when you reach 5th level (2d12)','Warlock'),(9,'Mage Hand','VS','None','Cantrip','A spectral, floating hand appears at a point you choose within range. The hand lasts for the duration or until you dismiss it as an action. The hand vanishes if it is ever more than 30 feet away from you or if you cast this spell again. You can use your action to control the hand. You can use the hand to manipulate an object, open an unlocked door or container, stow or retrieve an item from an open container, or pour the contents out of a vial. You can move the hand up to 30 feet each time you use it. The hand can\'t attack, activate magic items, or carry more than 10 pounds.','Bard'),(10,'Message','VS','None','Cantrip','You point your finger toward a creature within range and whisper a message. The target (and only the target) hears the message and can reply in a whisper that only you can hear. You can cast this spell through solid objects if you are familiar with the target and know it is beyond the barrier. Magical silence, 1 foot of stone, 1 inch of common metal, a thin sheet of lead, or 3 feet of wood blocks the spell. The spell doesn\'t have to follow a straight line and can travel freely around corners or through openings.','Bard'),(11,'None','None','None','None','None','None'),(12,'None1','None1','None1','None1','None1','None1');
/*!40000 ALTER TABLE `cantrip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characterfinal`
--

DROP TABLE IF EXISTS `characterfinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `characterfinal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `hp` int(11) DEFAULT NULL,
  `clase` varchar(45) DEFAULT NULL,
  `subclass` varchar(45) DEFAULT NULL,
  `race` varchar(45) DEFAULT NULL,
  `alignment` varchar(45) DEFAULT NULL,
  `background` varchar(100) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `str` int(11) DEFAULT NULL,
  `dex` int(11) DEFAULT NULL,
  `con` int(11) DEFAULT NULL,
  `inte` int(11) DEFAULT NULL,
  `wis` int(11) DEFAULT NULL,
  `cha` int(11) DEFAULT NULL,
  `athletics` int(11) DEFAULT NULL,
  `acrobatics` int(11) DEFAULT NULL,
  `sleightofhand` int(11) DEFAULT NULL,
  `stealth` int(11) DEFAULT NULL,
  `arcana` int(11) DEFAULT NULL,
  `history` int(11) DEFAULT NULL,
  `investigation` int(11) DEFAULT NULL,
  `nature` int(11) DEFAULT NULL,
  `religion` int(11) DEFAULT NULL,
  `animalhandling` int(11) DEFAULT NULL,
  `insight` int(11) DEFAULT NULL,
  `medicine` int(11) DEFAULT NULL,
  `perception` int(11) DEFAULT NULL,
  `survival` int(11) DEFAULT NULL,
  `deception` int(11) DEFAULT NULL,
  `intimidation` int(11) DEFAULT NULL,
  `performance` int(11) DEFAULT NULL,
  `persuasion` int(11) DEFAULT NULL,
  `savingstr` int(11) DEFAULT '0',
  `savingdex` int(11) DEFAULT '0',
  `savingcon` int(11) DEFAULT '0',
  `savinginte` int(11) DEFAULT '0',
  `savingwis` int(11) DEFAULT '0',
  `savingcha` int(11) DEFAULT '0',
  `proficiency` int(11) DEFAULT '0',
  `armorclass` int(11) DEFAULT '0',
  `initiative` int(11) DEFAULT '0',
  `speed` int(11) DEFAULT '0',
  `hitmax` int(11) DEFAULT '0',
  `currhit` varchar(45) DEFAULT '0',
  `temphit` varchar(45) DEFAULT '0',
  `hitdice` varchar(45) DEFAULT '0',
  `attspell` varchar(45) DEFAULT 'Info Here',
  `passwis` int(11) DEFAULT '0',
  `otherprof` varchar(45) DEFAULT 'Info here',
  `equip` varchar(45) DEFAULT 'Equip Here',
  `exppoint` varchar(45) DEFAULT 'exp',
  `feature` varchar(45) DEFAULT 'Info here',
  `img` varchar(45) DEFAULT 'wizard_icon.jpeg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characterfinal`
--

LOCK TABLES `characterfinal` WRITE;
/*!40000 ALTER TABLE `characterfinal` DISABLE KEYS */;
INSERT INTO `characterfinal` VALUES (1,'Oscar el sabio',1,'Warlock','eterno','Elfo','Neutral Good','Si',1,1,16,16,16,16,16,16,1,12,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,10,0,0,0,0,0,0,12,10,30,0,'0','0','0','112',0,'Gerard','Equip Hereasd','exp','Info here','warlock_icon.jpeg'),(6,'Ivan el travieso',17,NULL,NULL,NULL,'Lawful Neutral','Si',33,160,12,12,12,12,0,12,1,1,0,1,1,1,1,1,1,0,10,10,10,10,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,'0','0','0','Info Here',0,'Info here','Equip Here','exp','Info here','wizard_icon.jpeg'),(12,'Alberto el Piadoso',22,NULL,NULL,'Dwarf','Lawful Good','Back',33,190,16,16,14,16,12,16,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,'0','0','0','Info Here',0,'Info here','Equip Here','exp','Info here','wizard_icon.jpeg'),(26,'Isaac el Marc',13,'Fighter',NULL,'Human','Neutral','Isaac',33,180,14,16,14,16,12,13,2,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,0,2,3,2,3,1,1,2,10,3,30,15,'15','15','1d2','Info Here',11,'Info Here','Equip Here','exp','Isaac Marc Borras Vico','fighter_icon.jpeg'),(35,'Sergi el Poruc',18,'Cleric',NULL,'Human','Neutral Evil','Noble',42,178,12,17,10,6,20,15,1,3,3,3,-2,-2,-2,-2,-2,5,5,5,5,5,2,2,2,0,1,3,0,-2,5,2,2,10,3,30,15,'15','15','1d2','Info Here',15,'Info Here','Equip Here','exp','Info Here','cleric_icon.jpeg');
/*!40000 ALTER TABLE `characterfinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `characteroltankard`
--

DROP TABLE IF EXISTS `characteroltankard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `characteroltankard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `hp` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `subclassid` int(11) NOT NULL,
  `race` int(11) NOT NULL,
  `alignment` int(11) NOT NULL,
  `background` mediumtext CHARACTER SET cp1256 NOT NULL,
  `age` int(11) NOT NULL,
  `size` double NOT NULL,
  `primary_character` int(11) NOT NULL,
  `skillscharacter` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `race_idx` (`race`),
  KEY `subclassid_idx` (`subclassid`),
  KEY `class_idx` (`class`),
  KEY `skills_idx` (`skillscharacter`),
  KEY `primaryStats_idx` (`primary_character`),
  CONSTRAINT `class` FOREIGN KEY (`class`) REFERENCES `clases` (`id`),
  CONSTRAINT `primaryStats` FOREIGN KEY (`primary_character`) REFERENCES `primary_stats` (`id`),
  CONSTRAINT `race` FOREIGN KEY (`race`) REFERENCES `races` (`id`),
  CONSTRAINT `skills` FOREIGN KEY (`skillscharacter`) REFERENCES `skills` (`id`),
  CONSTRAINT `subclassid` FOREIGN KEY (`subclassid`) REFERENCES `subclass` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `characteroltankard`
--

LOCK TABLES `characteroltankard` WRITE;
/*!40000 ALTER TABLE `characteroltankard` DISABLE KEYS */;
INSERT INTO `characteroltankard` VALUES (3,'oscar',10,16,1,1,1,'si',1,1,1,1);
/*!40000 ALTER TABLE `characteroltankard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clases`
--

DROP TABLE IF EXISTS `clases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` mediumtext NOT NULL,
  `attack` varchar(250) NOT NULL,
  `subclass1` int(11) NOT NULL,
  `subclass2` int(11) NOT NULL,
  `spell1` int(11) NOT NULL,
  `spell2` int(11) NOT NULL,
  `cantrip1` int(11) NOT NULL,
  `cantrip2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `spell1_idx` (`spell1`),
  KEY `spell2_idx` (`spell2`),
  KEY `cantrip1_idx` (`cantrip1`),
  KEY `cantrip2_idx` (`cantrip2`),
  KEY `subclass1_idx` (`subclass1`),
  KEY `subclass2_idx` (`subclass2`),
  CONSTRAINT `cantrip1` FOREIGN KEY (`cantrip1`) REFERENCES `cantrip` (`id`),
  CONSTRAINT `cantrip2` FOREIGN KEY (`cantrip2`) REFERENCES `cantrip` (`id`),
  CONSTRAINT `spell1` FOREIGN KEY (`spell1`) REFERENCES `spells` (`id`),
  CONSTRAINT `spell2` FOREIGN KEY (`spell2`) REFERENCES `spells` (`id`),
  CONSTRAINT `subclass1` FOREIGN KEY (`subclass1`) REFERENCES `subclass` (`id`),
  CONSTRAINT `subclass2` FOREIGN KEY (`subclass2`) REFERENCES `subclass` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clases`
--

LOCK TABLES `clases` WRITE;
/*!40000 ALTER TABLE `clases` DISABLE KEYS */;
INSERT INTO `clases` VALUES (15,'Barbarian','A fierce warrior of primitive background who can enter a battle rage','1d10',1,2,3,3,11,12),(16,'Bard','An inspiring magician whose power echoes the music of creation','1d6',3,4,5,6,9,10),(17,'Cleric','A priestly champion who wields divine magic in service of higher power','1d6',5,6,7,8,3,4),(18,'Druid','A priest of the old Faith, wielding the powers of nature and adopting animal forms','1d6',7,8,9,10,11,12),(19,'Fighter','A master of martial combat, skilled with a variety of weapons and armor','1d10',9,10,1,2,11,12),(20,'Monk','A master of martial arts, harnessing the power of the body in pursuit of phyisical and spiritual perfection','1d6',11,12,11,12,11,12),(21,'Paladin','A holy warrior bound to a sacred oath.','1d10',13,14,13,14,11,12),(22,'Ranger','A warrior who combats threats on the edges of civilization.','1d9',15,16,15,16,11,12),(23,'Rogue','A scoundrel who uses stealth and trickery to overcome obstacles and enemies.','1d6',17,18,17,18,11,12),(24,'Sorcerer','A spellcaster who draws on inherent magic from a gift or bloodline.','1d4',19,20,19,20,5,6),(25,'Warlock','A wielder of magic that is derived from a bargain with an extraplanar entity','1d4',21,22,21,22,7,8),(26,'Wizard','A scholarly magic user capable of manipulating the structures of reality','1d4',23,24,23,24,1,2);
/*!40000 ALTER TABLE `clases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'Foto 1'),(2,'Foto 2'),(3,'Foto 3'),(4,'Foto 4'),(5,'Foto 5');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`(191))
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Book of the first archanist',100),(2,'Flask of the chosen',250),(3,'Clothes of the western realms',25),(4,'Boots of wisdom',55),(5,'Ink pen',15),(6,'Holy symbol',30);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maps`
--

DROP TABLE IF EXISTS `maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maps`
--

LOCK TABLES `maps` WRITE;
/*!40000 ALTER TABLE `maps` DISABLE KEYS */;
INSERT INTO `maps` VALUES (1,'Mapa1','Mapa inicial de la aventura de Sergi',13122001),(2,'Mapa2','Mapa inicial de la aventura de Dani',5062001),(3,'Mapa3','Mapa inicial de la aventura de Oscar',9032004),(4,'Mapa4','Mapa inicial de la aventura de Pau',14052003),(5,'Mapa5','Mapa inicial de la aventura de David',16092002);
/*!40000 ALTER TABLE `maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monsters`
--

DROP TABLE IF EXISTS `monsters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `monsters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `armor_class` int(11) DEFAULT NULL,
  `hit_points` int(11) DEFAULT NULL,
  `attack` varchar(250) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `str` int(11) DEFAULT NULL,
  `dex` int(11) DEFAULT NULL,
  `con` int(11) DEFAULT NULL,
  `inte` int(11) DEFAULT NULL,
  `wis` int(11) DEFAULT NULL,
  `cha` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monsters`
--

LOCK TABLES `monsters` WRITE;
/*!40000 ALTER TABLE `monsters` DISABLE KEYS */;
INSERT INTO `monsters` VALUES (2,'Dragon Turtle','Gargantuan dragon, neutral',20,341,'1d12',20,25,10,20,10,12,12),(3,'Gargoyle','Medium elemental, chaotic evil',15,52,'1d6',30,15,11,16,6,11,7),(4,'Gorgon','Large monstrosity, unaligned',19,114,'1d8',40,20,11,18,2,12,7),(5,'Hydra','Huge monstrosity, unaligned',15,172,'1d12',30,20,12,20,2,10,7),(6,'Kraken','Gargantuan monstrosity (titan), chaotic evil',18,472,'1d20',20,30,11,25,22,18,20),(7,'Mimic','Medium monstrosity (shapechanger), neutral',12,58,'1d6',15,17,12,15,5,13,8),(8,'Oni','Large giant, lawful evil',16,110,'1d8',30,19,11,16,14,12,15),(9,'Rug Of Smothering','Large construct, unaligned',12,33,'1d6',10,17,14,10,1,3,1),(10,'Will-O-Wisp','Tiny undead, chaotic evil',19,22,'1d4',50,1,28,10,13,14,11),(56,'Marc','Borras',12,12,'12',12,12,12,12,12,12,12);
/*!40000 ALTER TABLE `monsters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `text` mediumtext,
  `date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES (1,'Primera nota','Esta texto es de prueba numero uno',9122001),(3,'Tercera nota','Esta texto es de prueba numero tres',8092009);
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes_master`
--

DROP TABLE IF EXISTS `notes_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notes_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `text` mediumtext,
  `date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes_master`
--

LOCK TABLES `notes_master` WRITE;
/*!40000 ALTER TABLE `notes_master` DISABLE KEYS */;
INSERT INTO `notes_master` VALUES (27,'a',NULL,0),(28,'a',NULL,0),(29,'asd','EL marc es tonto',0),(30,'fasf','as',0),(35,'Gerad','asdassd',0);
/*!40000 ALTER TABLE `notes_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `primary_stats`
--

DROP TABLE IF EXISTS `primary_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `primary_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `str` int(11) DEFAULT NULL,
  `dex` int(11) DEFAULT NULL,
  `con` int(11) DEFAULT NULL,
  `inte` int(11) DEFAULT NULL,
  `wis` int(11) DEFAULT NULL,
  `cha` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `primary_stats`
--

LOCK TABLES `primary_stats` WRITE;
/*!40000 ALTER TABLE `primary_stats` DISABLE KEYS */;
INSERT INTO `primary_stats` VALUES (1,12,12,12,12,12,12),(2,12,12,12,12,12,12),(3,12,12,12,12,12,12),(4,12,12,12,12,12,12),(5,12,12,12,12,12,12),(6,0,0,0,0,0,0),(7,1,1,1,1,1,1),(8,0,0,0,0,0,0),(9,0,0,0,0,0,0),(10,0,0,0,0,0,0),(11,0,0,0,0,0,0),(12,0,0,0,0,0,0),(13,0,0,0,0,0,0);
/*!40000 ALTER TABLE `primary_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'PT01','Producto de test','2018-12-31 22:00:00','2019-08-02 06:50:31'),(2,'PT02','Producto de test','2018-12-31 22:00:00','2019-07-31 20:00:00'),(3,'PT03','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(4,'PT04','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(5,'PT05','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(6,'PT06','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(7,'PT07','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(8,'PT08','Producto de test','2019-08-01 05:40:09','2019-08-01 05:40:09'),(9,'PT09','Producto de test','2018-12-31 22:00:00','2018-12-31 22:00:00'),(10,'PT10','Producto de test','2019-08-02 05:34:42','2019-08-02 05:34:42'),(11,'PT11','Producto de test','2019-08-02 06:49:07','2019-08-02 06:49:07'),(12,'PT10','Producto de test','2019-09-04 15:02:53','2019-09-04 15:02:53'),(15,'PT99','Producto de test AJAX','2019-09-04 15:32:16','2019-09-04 15:32:16'),(16,'PT99','Producto de test AJAX','2019-09-04 16:13:09','2019-09-04 16:13:09');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `races`
--

DROP TABLE IF EXISTS `races`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `races` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `Speed` varchar(250) NOT NULL,
  `Description` mediumtext NOT NULL,
  `AbilityScore` mediumtext NOT NULL,
  `Trait1` mediumtext NOT NULL,
  `Trait2` mediumtext NOT NULL,
  `Trait3` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `races`
--

LOCK TABLES `races` WRITE;
/*!40000 ALTER TABLE `races` DISABLE KEYS */;
INSERT INTO `races` VALUES (1,'Dragonborn','30','Born of dragons, as their name proclaims, the dragonborn walk proudly through a world that greets them with fearful incomprehension. Shaped by draconic gods or the dragons themselves, dragonborn originally hatched from dragon eggs as a unique race, combining the best attributes of dragons and humanoids.','Your Strength score increases by 2 and your Charisma by 1','Draconic ancestry','Damage resistance','Breath weapon'),(2,'Dwarf','25','Kingdoms rich in ancient grandeur, halls carved into the roots of mountains, the echoing of picks and hammers in deep mines and blazing forges, a commitment to clan and tradition, and a burning hatred of goblins and orcs—these common threads unite all dwarves.','Your Constitution score increases by 2','Darkvision','Dwarven Resilence','Dwarven Combat Training'),(3,'Elf','30','Elves are a magical people of otherworldly grace, living in the world but not entirely part of it. They live in places of ethereal beauty, in the midst of ancient forests or in silvery spires glittering with faerie light, where soft music drifts through the air and gentle fragrances waft on the breeze.','Your Dexterity score increases by 2','Darkvision','Keen senses','Fey ancestry'),(4,'Gnome','25','A constant hum of busy activity pervades the warrens and neighborhoods where gnomes form their close-knit communities. Louder sounds punctuate the hum: a crunch of grinding gears here, a minor explosion there, a yelp of surprise or triumph, and especially bursts of laughter.','Your Dexterity score increases by 1','Gnome Cunning','Natural illusionist','Speak with small beasts'),(5,'Half-Elf','30','Walking in two worlds but truly belonging to neither, half-elves combine what some say are the best qualities of their elf and human parents: human curiosity, inventiveness, and ambition tempered by the refined senses, love of nature, and artistic tastes of the elves.','Your Charisma score increses by 2, and two other ability scores of your choice increase by 1','Darkvision','Fey ancestry','Skill versatility'),(6,'Halfling','30','The comforts of home are the goals of most halflings’ lives: a place to settle in peace and quiet, far from marauding monsters and clashing armies; a blazing fire and a generous meal; fine drink and fine conversation.','Your Dexterity score increases by 2','Lucky','Brave','Halfling Nimbleness'),(7,'Half-Orc','30','Whether united under the leadership of a mighty warlock or having fought to a standstill after years of conflict, orc and human tribes sometimes form alliances, joining forces into a larger horde to the terror of civilized lands nearby. When these alliances are sealed by marriages, half-orcs are born.','Your Strength score increases by 2 and your Constitution by 1','Gnome Cunning','Natural illusionist','Speak with small beasts'),(8,'Human','30','In the reckonings of most worlds, humans are the youngest of the common races, late to arrive on the world scene and short-lived in comparison to dwarves, elves, and dragons. Perhaps it is because of their shorter lives that they strive to achieve as much as they can in the years they are given.','Your ability scores increse by 1','Ability Score','Skills','Feat'),(9,'Tiefling','30','To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling. And to twist the knife, tieflings know that this is because a pact struck generations ago infused the essence of Asmodeus—overlord of the Nine Hells—into their bloodline.','Your Intelligence score increases by 1 and your Charisma by 2','Hellish resistance','Infernal legacy','Darkvision');
/*!40000 ALTER TABLE `races` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `athletics` int(11) DEFAULT NULL,
  `acrobatics` int(11) DEFAULT NULL,
  `sleight_of_hand` int(11) DEFAULT NULL,
  `stealth` int(11) DEFAULT NULL,
  `arcana` int(11) DEFAULT NULL,
  `history` int(11) DEFAULT NULL,
  `investigation` int(11) DEFAULT NULL,
  `nature` int(11) DEFAULT NULL,
  `religion` int(11) DEFAULT NULL,
  `animalHandling` int(11) DEFAULT NULL,
  `insight` int(11) DEFAULT NULL,
  `medicine` int(11) DEFAULT NULL,
  `perception` int(11) DEFAULT NULL,
  `survival` int(11) DEFAULT NULL,
  `deception` int(11) DEFAULT NULL,
  `intimidation` int(11) DEFAULT NULL,
  `performance` int(11) DEFAULT NULL,
  `persuasion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spells`
--

DROP TABLE IF EXISTS `spells`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `Type` varchar(250) DEFAULT NULL,
  `Dice` varchar(250) DEFAULT NULL,
  `Slot` varchar(250) DEFAULT NULL,
  `Description` mediumtext,
  `referencedClass` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spells`
--

LOCK TABLES `spells` WRITE;
/*!40000 ALTER TABLE `spells` DISABLE KEYS */;
INSERT INTO `spells` VALUES (1,'Strike','S','1d8','None','When making a ranged attack while you are within 5 feet of a hostile creature, you do not have disadvantage on the attack roll. Your ranged attacks ignore half cover and three-quarters cover against targets within 30 feet of you.','Warrior'),(2,'Second wind','S','None','None','You have a limited well of stamina that you can draw on to protect yourself from harm. On your turn, you can use a bonus action to regain hit points equal to 1d10 + your fighter level. ','Warrior'),(3,'Rage','S','None','None','You have advantage on Strength checks and Strength saving throws','Barbarian'),(4,'Unarmored Defense','M','None','None','While you are not wearing any armor, your armor class equals 10 + your Dexterity modifier + your Constitution modifier. You can use a shield and still gain this benefit.','Barbarian'),(5,'Charm Person','VS','None','Spell slot 1','You attempt to charm a humanoid you can see within range. It must make a Wisdom saving throw, and does so with advantage if you or your companions are fighting it. If it fails the saving throw, it is charmed by you until the spell ends or until you or your companions do anything harmful to it. The charmed creature regards you as a friendly acquaintance. When the spell ends, the creature knows it was charmed by you.','Bard'),(6,'True strike','S','1d8','Spell slot 1','You extend your hand and point a finger at a target in range. Your magic grants you a brief insight into the target\'s defenses. On your next turn, you gain advantage on your first attack roll against the target, provided that this spell hasn\'t ended.','Bard'),(7,'Cure wounds','VS','1d8','Spell slot 1','A creature you touch regains a number of hit points equal to 1d8 + your spellcasting ability modifier. This spell has no effect on undead or constructs.','Cleric'),(8,'Inflict wounds','VS','1d10','Spell slot 1','Make a melee spell attack against a creature you can reach. On a hit, the target takes 1d10 necrotic damage.','Cleric'),(9,'Cure Wounds','VS','1d8','Spell slot 1','A creature you touch regains a number of hit points equal to 1d8 + your spellcasting ability modifier. This spell has no effect on undead or constructs.','Druid'),(10,'Thunderwave','VS','2d8','Spell slot 1','A wave of thunderous force sweeps out from you. Each creature in a 15-foot cube originating from you must make a Constitution saving throw. On a failed save, a creature takes 2d8 thunder damage and is pushed 10 feet away from you. On a successful save, the creature takes half as much damage and isn\'t pushed.','Druid'),(11,'Flurry of Blows','VS','1d6','Spell slot 1','Immediately after you take the Attack action on your turn, you can spend 1 ki point to make two unarmed strikes as a bonus action','Monk'),(12,'Patient Defense','VS','None','Spell slot 1','You can spend 1 ki point to take the Dodge action as a bonus action on your turn.','Monk'),(13,'Cure Wounds','VS','1d8','Spell slot 1','A creature you touch regains a number of hit points equal to 1d8 + your spellcasting ability modifier. This spell has no effect on undead or constructs.','Paladin'),(14,'Shield of Faith','VSM','1d8','Spell slot 1','A shimmering field appears and surrounds a creature of your choice within range, granting it a +1d8 bonus to AC for the duration.','Paladin'),(15,'Hunters Mark','V','None','Spell slot 1','You choose a creature you can see within range and mystically mark it as your quarry. Until the spell ends, you deal an extra 1d6 damage to the target whenever you hit it with a weapon attack, and you have advantage on any Wisdom (Perception) or Wisdom (Survival) check you make to find it. If the target drops to 0 hit points before this spell ends, you can use a bonus action on a subsequent turn of yours to mark a new creature.','Ranger'),(16,'Detect Magic','VS','None','Spell slot 1','For the duration, you sense the presence of magic within 30 feet of you. If you sense magic in this way, you can use your action to see a faint aura around any visible creature or object in the area that bears magic, and you learn its school of magic.','Ranger'),(17,'Sneak Attack','S','1d6','Spell slot 1','Beginning at 1st level, you know how to strike subtly and exploit a foe’s distraction. Once per turn, you can deal an extra 1d6 damage to one creature you hit with an attack if you have advantage on the attack roll. The attack must use a finesse or a ranged weapon.','Rogue'),(18,'Uncanny Dodge','S','None','Spell slot 1','Starting at 5th level, when an attacker that you can see hits you with an attack, you can use your reaction to halve the attack’s damage against you.','Rogue'),(19,'Earth Tremor','VS','1d6','Spell slot 1','You cause a tremor in the ground within range. Each creature other than you in that area must make a Dexterity saving throw. On a failed save, a creature takes 1d6 bludgeoning damage and is knocked prone. If the ground in that area is loose earth or stone, it becomes difficult terrain until cleared, with each 5-foot-diameter portion requiring at least 1 minute to clear by hand.','Sorcerer'),(20,'Feather Fall','VM','None','Spell slot 1','Choose up to five falling creatures within range. A falling creature\'s rate of descent slows to 60 feet per round until the spell ends. If the creature lands before the spell ends, it takes no falling damage and can land on its feet, and the spell ends for that creature.','Sorcerer'),(21,'Charm Person','VS','None','Spell slot 1','You attempt to charm a humanoid you can see within range. It must make a Wisdom saving throw, and does so with advantage if you or your companions are fighting it. If it fails the saving throw, it is charmed by you until the spell ends or until you or your companions do anything harmful to it. The charmed creature regards you as a friendly acquaintance. When the spell ends, the creature knows it was charmed by you.','Warlock'),(22,'Hellish Rebuke','VS','2d10','Spell slot 1','You point your finger, and the creature that damaged you is momentarily surrounded by hellish flames. The creature must make a Dexterity saving throw. It takes 2d10 fire damage on a failed save, or half as much damage on a successful one.','Warlock'),(23,'Mage Armor','VSM','None','Spell slot 1','You touch a willing creature who isn\'t wearing armor, and a protective magical force surrounds it until the spell ends. The target\'s base AC becomes 13 + its Dexterity modifier. The spell ends if the target dons armor or if you dismiss the spell as an action.','Wizard'),(24,'Magic Missile','VS','1d4','Spell slot 1','You create three glowing darts of magical force. Each dart hits a creature of your choice that you can see within range. A dart deals 1d4 + 1 force damage to its target. The darts all strike simultaneously, and you can direct them to hit one creature or several.','Wizard');
/*!40000 ALTER TABLE `spells` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subclass`
--

DROP TABLE IF EXISTS `subclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `Description` mediumtext NOT NULL,
  `Trait1` mediumtext NOT NULL,
  `Trait1Desc` mediumtext NOT NULL,
  `Trait2` mediumtext NOT NULL,
  `Trait2Desc` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subclass`
--

LOCK TABLES `subclass` WRITE;
/*!40000 ALTER TABLE `subclass` DISABLE KEYS */;
INSERT INTO `subclass` VALUES (1,'Path of the berserker','For some barbarians, rage is a means to an end that\nend being violence. The Path of the Berserker is a path of untrammeled fury, slick with blood.','Frenzy','Starting w hen you choose this path at 3rd level, you\ncan go into a frenzy when you rage. If you do so, for\nthe duration of your rage you can make a single melee\nw eapon attack as a bonus action on each of your turns\nafter this one.','Mindless Rage','Beginning at 6th level, you can’t be charm ed or\nfrightened while raging. If you are charm ed or\nfrightened when you enter your rage, the effect is\nsuspended for the duration of the rage.'),(2,'Path of the totem warrior','The Path of the Totem W arrior is a spiritual journey, as\nthe barbarian accepts a spirit animal as guide, protector,\nand inspiration. In battle, your totem spirit fills you\nwith supernatural might, adding magical fuel to your\nbarbarian rage','Totem Spirit','At 3rd level, when you adopt this path, you choose a\ntotem spirit and gain its feature. You must make or\nacquire a physical totem object an amulet or similar\nadornment that incorporates fur or feathers, claws,\nteeth, or bones of the totem animal','Aspect of the Beast','At 6th level, you gain a m agical benefit based on the\ntotem animal of your choice. You can choose the same\nanimal you selected at 3rd level or a different one.'),(3,'College of lore','The way of a bard is gregarious. Bards seek each\nother out to swap songs and stories, boast o f their\naccom plishm ents, and share their knowledge.','Cutting words','A lso at 3rd level, you learn how to use your wit to\ndistract, confuse, and otherwise sap the confidence and\ncom petence of others. W hen a creature that you can\nsee within 60 feet of you makes an attack roll, an ability\ncheck, or a damage roll, you can use your reaction to\nexpend one of your uses of Bardic Inspiration, rolling\na Bardic Inspiration die and subtracting the number\nrolled from the creature’s roll.','Additional magical secrets','At 6th level, you learn two spells of your choice from any\nclass. A spell you choose must be of a level you can cast,\nas shown on the Bard table, or a cantrip. '),(4,'College of valor','Bards of the College of Valor are daring skalds w hose\ntales keep alive the m em ory of the great heroes of the\npast, and thereby inspire a new generation of heroes.\nThese bards gather in mead halls or around great\nbonfires to sing the deeds of the mighty, both past\nand present.','Combat inspiration','Also at 3rd level, you learn to inspire others in battle.\nA creature that has a Bardic Inspiration die from you\ncan roll that die and add the number rolled to a weapon\ndamage roll it just made. ','Extra attack','Starting at 6th level, you can attack twice, instead of\nonce, whenever you take the Attack action on your turn'),(5,'Life domain','The Life dom ain focuses on the vibrant positive\nenergy—one o f the fundamental forces of the universe—\nthat sustains all life. The gods o f life prom ote vitality\nand health through healing the sick and wounded,\ncaring for those in need, and driving away the forces of\ndeath and undeath.','Channel divinity: preserve life','Starting at 2nd level, you can use your Channel Divinity\nto heal the badly injured.\nAs an action, you present your holy sym bol and\nevoke healing energy that can restore a number of hit\npoints equal to five times your cleric level.','Blessed healer','Beginning at 6th level, the healing spells you cast on\nothers heal you as well. W hen you cast a spell of 1st\nlevel or higher that restores hit points to a creature\nother than you, you regain hit points equal to 2 + the\nspell’s level.'),(6,'Light domain','Gods of light—including Helm, Lathander, Pholtus,\nBranchala, the Silver Flame, Belenus, Apollo, and\nRe-Horakhty—prom ote the ideals of rebirth and\nrenewal, truth, vigilance, and beauty, often using the\nsymbol of the sun','Channel divinity: radiance of the dawn','Starting at 2nd level, you can use your Channel Divinity\nto harness sunlight, banishing darkness and dealing\nradiant damage to your foes.\nAs an action, you present your holy symbol, and any\nm agical darkness within 30 feet of you is dispelled. ','Improved flare','Starting at 6th level, you can also use your W arding\nFlare feature when a creature that you can see within\n30 feet o f you attacks a creature other than you.\n'),(7,'Circle of the land','The Circle of the Land is made up of mystics and sages\nw ho safeguard ancient knowledge and rites through\na vast oral tradition. These druids meet within sacred\ncircles of trees or standing stones to whisper primal\nsecrets in Druidic. ','Bonus cantrip','When you choose this circle at 2nd level, you learn one\nadditional druid cantrip of your choice','Circle spells','Your mystical connection to the land infuses you with\nthe ability to cast certain spells. At 3rd, 5th, 7th, and\n9th level you gain access to circle spells connected\nto the land where you becam e a druid. '),(8,'Circle of the moon','Druids of the Circle of the M oon are fierce guardians\nof the wilds. Their order gathers under the full moon to\nshare news and trade warnings. They haunt the deepest\nparts of the wilderness, where they might go for w eeks\non end before crossing paths with another humanoid\ncreature, let alone another druid','Combat wild shape','When you ch oose this circle at 2nd level, you gain the\nability to use W ild Shape on your turn as a bonus action,\nrather than as an action.\n','Primal strike','Starting at 6th level, your attacks in beast form count as\nm agical for the purpose of overcom ing resistance and\nimmunity to nonm agical attacks and damage.\n'),(9,'Champion','The archetypal Champion focuses on the development\nof raw physical pow er honed to deadly perfection.\nThose who model themselves on this archetype combine\nrigorous training with physical excellence to deal\ndevastating blows.','Improved critical','Beginning when you ch oose this archetype at 3rd\nlevel, your w eapon attacks score a critical hit on a\nroll of 19 or 20.\n','Remarkable athlete','Starting at 7th level, you can add half your proficiency\nbonus (round up) to any Strength, Dexterity, or\nConstitution check you make that doesn’t already use\nyour proficiency bonus'),(10,'Battle master','Those who emulate the archetypal Battle Master\nemploy martial techniques passed down through\ngenerations. To a Battle Master, com bat is an academ ic\nfield, som etim es including subjects beyond battle such\nas weaponsm ithing and calligraphy. ','Combat superiority','When you ch oose this archetype at 3rd level, you\nlearn maneuvers that are fueled by special dice called\nsuperiority dice.','Know your enemy','Starting at 7th level, if you spend at least 1 minute\nobserving or interacting with another creature outside\ncombat, you can learn certain information about its\ncapabilities com pared to your own. '),(11,'Way of the open hand','Monks of the Way of the Open Hand are the ultimate\nmasters of martial arts combat, whether armed or\nunarm ed.','Open hand techique','Starting w hen you choose this tradition at 3rd level,\nyou can manipulate your enem y’s ki w hen you harness\nyour own. ','Wholeness of body','At 6th level, you gain the ability to heal yourself. As an\naction, you can regain hit points equal to three times your m onk level. You must finish a long rest before you\ncan use this feature again.'),(12,'Way of the shadow','Monks of the Way of Shadow follow a tradition that\nvalues stealth and subterfuge. These m onks might\nbe called ninjas or shadowdancers, and they serve as\nspies and assassins. ','Shadow arts','Starting when you choose this tradition at 3rd level, you\ncan use your ki to duplicate the effects of certain spells.\nAs an action, you can spend 2 ki points to cast darkness,\ndarkvision, pass without trace, or silence, without\nproviding material com ponents.','Shadow step','At 6th level, you gain the ability to step from one shadow\ninto another. W hen you are in dim light or darkness,\nas a bonus action you can teleport up to 60 feet to an\nunoccupied space you can see that is also in dim light\nor darkness.'),(13,'Oath of devotion','The Oath of Devotion binds a paladin to the loftiest\nideals of justice, virtue, and order. Som etim es called\ncavaliers, white knights, or holy warriors, these\npaladins meet the ideal of the knight in shining armor,\nacting with honor in pursuit o f justice and the greater\ngood. ','Aura of devotion','Starting at 3th level, you and friendly creatures within\n10 feet o f you can’t be charm ed w hile you are conscious.\nAt 18th level, the range of this aura increases to 30 feet.\n','Purity of spirit','Beginning at 6th level, you are always under the effects\nof a protection from evil and good spell.'),(14,'Oath of the ancients','The Oath of the Ancients is as old as the race of elves\nand the rituals of the druids. Som etim es called fey\nknights, green knights, or horned knights, paladins w ho\nswear this oath cast their lot with the side of the light\nin the cosm ic struggle against darkness because they\nlove the beautiful and life-giving things of the world','Aura of warding','Beginning at 3th level, ancient m agic lies so heavily\nupon you that it form s an eldritch ward. You and friendly\ncreatures within 10 feet of you have resistance to\ndamage from spells.','Undying sentinel','Starting at 6th level, when you are reduced to 0 hit\npoints and are not killed outright, you can choose to\ndrop to 1 hit point instead. Once you use this ability, you\ncan’t use it again until you finish a long rest.'),(15,'Hunter','Emulating the Hunter archetype m eans accepting your\nplace as a bulwark between civilization and the terrors\nof the wilderness. As you walk the Hunter’s path, you\nlearn specialized techniques for fighting the threats\nyou face, from rampaging ogres and hordes of orcs to\ntowering giants and terrifying dragons.','Hunter prey','At 3rd level, you gain one  feature:\nColossus Slayer. Your tenacity can w ear down\nthe m ost potent foes. W hen you hit a creature with a\nweapon attack, the creature takes an extra 1d8 damage\nif it’s below its hit point maximum. You can deal this\nextra damage only once per turn.','Multiattack','At 6th level, you gain the following feature:\nVolley. You can use your action to make a ranged\nattack against any num ber of creatures within 10 feet\no f a point you can see within your weapon’s range. You\nmust have ammunition for each target, as normal, and\nyou make a separate attack roll for each target.'),(16,'Beast master','The Beast Master archetype em bodies a friendship\nbetween the civilized races and the beasts of the world.\nUnited in focus, beast and ranger w ork as one to fight\nthe m onstrous foes that threaten civilization and the\nw ilderness alike. ','Ranger companion','At 3rd level, you gain a beast com panion that\naccom panies you on your adventures and is trained to\nfight alongside you. Choose a beast that is no larger than\nMedium and that has a challenge rating of 1/4 or lower\n(appendix D presents statistics for the hawk, mastiff,\nand panther as examples). Add your proficiency bonus\nto the beast’s AC, attack rolls, and damage rolls, as well\nas to any saving throws and skills it is proficient in. ','Exceptional training','Beginning at 7th level, on any of your turns when\nyour beast com panion doesn’t attack, you can use a\nbonus action to com m and the beast to take the Dash,\nDisengage, Dodge, or Help action on its turn.'),(17,'Thief','You hone your skills in the larcenous arts. Burglars,\nbandits, cutpurses, and other crim inals typically follow\nthis archetype, but so do rogues who prefer to think of\nthemselves as professional treasure seekers, explorers,\ndelvers, and investigators.','Fast hands','Starting at 3rd level, you can use the bonus action\ngranted by your Cunning Action to make a Dexterity\n(Sleight of Hand) check, use your thieves’ tools to\ndisarm a trap or open a lock, or take the Use an\nObject action','Supreme sneak','Starting at 6th level, you have advantage on a Dexterity\n(Stealth) check if you move no more than half your\nspeed on the same turn'),(18,'Assassin','You focus your training on the grim art of death. Those\nw ho adhere to this archetype are diverse: hired killers,\nspies, bounty hunters, and even specially anointed\npriests trained to exterminate the enem ies of their deity. ','Assessinate','Starting at 3rd level, you are at your deadliest w hen you\nget the drop on your enemies. You have advantage on\nattack rolls against any creature that hasn’t taken a turn\nin the com bat yet.','Infiltration expertise','Starting at 6th level, you can unfailingly create false\nidentities for yourself. You must spend seven days and\n25 gp to establish the history, profession, and affiliations\nfor an identity. '),(19,'Draconic bloodline','Your innate m agic com es from draconic m agic that was\nmingled with your blood or that o f your ancestors. M ost\noften, sorcerers with this origin trace their descent\nback to a mighty sorcerer of ancient times w ho made a\nbargain with a dragon or who might even have claim ed\na dragon parent.','Dragon ancestor','At 3th level, you choose one type of dragon as your\nancestor. The damage type associated with each dragon\nis used by features you gain later.','Elemental affinity','Starting at 6th level, when you cast a spell that deals\ndamage of the type associated with your draconic\nancestry, add your Charisma modifier to that damage'),(20,'Wild magic','Your innate m agic com es from the wild forces of chaos\nthat underlie the order of creation. You might have\nendured exposure to som e form o f raw magic, perhaps\nthrough a planar portal leading to Limbo, the Elemental\nPlanes, or the mysterious Far Realm.','Tides of chaos','Starting at 3th level, you can manipulate the forces of\nchance and chaos to gain advantage on one attack roll,\nability check, or saving throw. O nce you do so, you must\nfinish a long rest before you can use this feature again.\n','Bend luck','Starting at 6th level, you have the ability to twist fate\nusing your w ild magic. W hen another creature you can\nsee m akes an attack roll, an ability check, or a saving\nthrow, you can use your reaction and spend 2 sorcery\npoints to roll 1d4 and apply the number rolled as a\nbonus or penalty (your choice) to the creature’s roll.'),(21,'The great old one','Your patron is a mysterious entity w hose nature is\nutterly foreign to the fabric of reality. It might com e from\nthe Far Realm, the space beyond reality, or it could be\none of the elder gods known only in legends. Its motives\nare incom prehensible to mortals, and its knowledge so\nim m ense and ancient that even the greatest libraries\npale in com parison to the vast secrets it holds. ','Awakaned mind','Starting at 3th level, your alien knowledge gives you\nthe ability to touch the minds of other creatures. You\ncan com m unicate telepathically with any creature you\ncan see within 30 feet of you. ','Entropic ward','At 6th level, you learn to magically ward yourself against\nattack and to turn an enem y’s failed strike into good\nluck for yourself. W hen a creature m akes an attack\nroll against you, you can use your reaction to im pose\ndisadvantage on that roll.'),(22,'The fiend','You have made a pact with a fiend from the lower\nplanes o f existence, a being w hose aims are evil,\neven if you strive against those aims. Such beings\ndesire the corruption or destruction of all things,\nultimately including you. ','Dark ones blessing','Starting at 3th level, when you reduce a hostile creature\nto 0 hit points, you gain temporary hit points equal to your\nCharisma m odifier + your warlock level (minimum of 1).','Dark ones own luck','Starting at 6th level, you can call on your patron to alter\nfate in your favor. W hen you make an ability check or a\nsaving throw, you can use this feature to add a d 10 to\nyour roll. You can do so after seeing the initial roll but\nbefore any o f the roll’s effects occur.'),(23,'School of abjuration','The School of Abjuration em phasizes m agic that blocks,\nbanishes, or protects. Detractors of this school say\nthat its tradition is about denial, negation rather than\npositive assertion. You understand, however, that ending\nharmful effects, protecting the weak, and banishing evil\ninfluences is anything but a philosophical void. ','Arcane ward','Starting at 3th level, you can weave m agic around\nyourself for protection. W hen you cast an abjuration\nspell of 1st level or higher, you can simultaneously use a\nstrand of the spell’s m agic to create a magical ward on\nyourself that lasts until you finish a long rest. The ward\nhas hit points equal to tw ice your wizard level + your\nIntelligence modifier. W henever you take damage, the\nward takes the damage instead. If this damage reduces\nthe ward to 0 hit points, you take any remaining damage.','Projected ward','Starting at 6th level, w hen a creature that you can see\nwithin 30 feet of you takes damage, you can use your\nreaction to cause your Arcane W ard to absorb that\ndamage.'),(24,'School of conjuration','As a conjurer, you favor spells that produce objects\nand creatures out o f thin air. You can conjure billowing\nclouds of killing fog or sum m on creatures from\nelsewhere to fight on your behalf.','Minor conjuration','Starting at 3th level when you select this school, you\ncan use your action to conjure up an inanimate object\nin your hand or on the ground in an unoccupied space\nthat you can see within 10 feet of you. ','Benign transposition','Starting at 6th level, you can use your action to teleport\nup to 30 feet to an unoccupied space that you can see.\nAlternatively, you can choose a space within range\nthat is occupied by a Sm all or M edium creature. ');
/*!40000 ALTER TABLE `subclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weapons`
--

DROP TABLE IF EXISTS `weapons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `weapons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) NOT NULL,
  `damage` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `properties` mediumtext,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`(191))
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weapons`
--

LOCK TABLES `weapons` WRITE;
/*!40000 ALTER TABLE `weapons` DISABLE KEYS */;
INSERT INTO `weapons` VALUES (1,'Spear',6,1,'Thrown 40 range, two-handed'),(2,'Shortbow',6,25,'Ammunition 320 range, two-handed'),(3,'Warhammer',8,15,'Versatile 1d10'),(4,'Pike',10,5,'Heavy, reach, two-handed'),(5,'Battleaxe',10,8,'Versatile 1d10'),(6,'Shortsword',6,10,'Finesse, light'),(7,'Sword of the western realms',6,10,'Finesse, light'),(8,'Scimitar',6,25,'Finesse, light'),(9,'Trident',6,5,'Thrown 60 range, two-handed'),(10,'Maul',12,10,'Heavy, two-handed');
/*!40000 ALTER TABLE `weapons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-18 17:48:45
